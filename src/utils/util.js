export function timeFix () {
  const time = new Date()
  const hour = time.getHours()
  return hour < 9 ? '早上好' : hour <= 11 ? '上午好' : hour <= 13 ? '中午好' : hour < 20 ? '下午好' : '晚上好'
}

export function welcome () {
  return '祝你开心每一天！'
}

/**
 * 触发 window.resize
 */
export function triggerWindowResizeEvent () {
  const event = document.createEvent('HTMLEvents')
  event.initEvent('resize', true, true)
  event.eventType = 'message'
  window.dispatchEvent(event)
}

export function handleScrollHeader (callback) {
  let timer = 0

  let beforeScrollTop = window.pageYOffset
  callback = callback || function () {}
  window.addEventListener(
    'scroll',
    event => {
      clearTimeout(timer)
      timer = setTimeout(() => {
        let direction = 'up'
        const afterScrollTop = window.pageYOffset
        const delta = afterScrollTop - beforeScrollTop
        if (delta === 0) {
          return false
        }
        direction = delta > 0 ? 'down' : 'up'
        callback(direction)
        beforeScrollTop = afterScrollTop
      }, 50)
    },
    false
  )
}

export function isIE () {
  const bw = window.navigator.userAgent
  const compare = (s) => bw.indexOf(s) >= 0
  const ie11 = (() => 'ActiveXObject' in window)()
  return compare('MSIE') || ie11
}

/**
 * Remove loading animate
 * @param id parent element id or class
 * @param timeout
 */
export function removeLoadingAnimate (id = '', timeout = 1500) {
  if (id === '') {
    return
  }
  setTimeout(() => {
    document.body.removeChild(document.getElementById(id))
  }, timeout)
}

/**
 * 下载
 * @param {*} fileName 文件名
 * @param {*} data 文件流
 */
export function download (fileName, data) {
  const blob = new Blob([data])
  if ('download' in document.createElement('a')) {
    const url = window.URL.createObjectURL(blob)
    const link = document.createElement('a')
    link.style.display = 'none'
    link.href = url
    link.setAttribute('download', fileName)
    document.body.appendChild(link)
    link.click()
    document.body.removeChild(link)
    window.URL.revokeObjectURL(url)
  }
}

/**
 * 递归树
 * @param {*} data 文件名
 * @param {*} pid 父级id
 */
export function tree (data, pid = 0, parentName = 'pid', idName = 'id') {
  const result = []
  for (const i in data) {
    if (data[i][parentName] === pid) {
      const temp = data[i]
      const children = tree(data, data[i][idName], parentName)
      if (children.length) {
        temp.children = children
      }
      result.push(temp)
    }
  }

  return result
}

/**
 * 生成一个数组
 */
 export const range = (start = 0, end = 10, step = 1) => {
  const arr = []
  for (let index = start; index < end;) {
    if (index <= end) {
      arr.push(index)
    }
    index = index + step
  }
  return arr
}

/**
 * 生成一个数组
 */
 export const toArray = (arr, split = ',') => {
   if (Array.isArray(arr)) {
     return arr
   }
   if (arr instanceof String) {
     return arr.split(split)
   }
   return []
}

export const findIdInArr = (arr, id, idName = 'id') => {
  for (let index = 0; index < arr.length; index++) {
    const el = arr[index]
    if (el[idName] === id) {
      return el
    }
  }
  return null
}
