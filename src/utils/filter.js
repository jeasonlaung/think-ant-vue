import Vue from 'vue'
import moment from 'moment'
import 'moment/locale/zh-cn'
moment.locale('zh-cn')

Vue.filter('NumberFormat', function (value) {
  if (!value) {
    return '0'
  }
  const intPartFormat = value.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,') // 将整数部分逢三一断
  return intPartFormat
})

Vue.filter('dayjs', function (dataStr, pattern = 'YYYY-MM-DD HH:mm:ss') {
  return moment(dataStr).format(pattern)
})

Vue.filter('moment', function (dataStr, pattern = 'YYYY-MM-DD HH:mm:ss') {
  return moment(dataStr).format(pattern)
})

Vue.filter('now', function (dataStr) {
  return moment(dataStr).fromNow()
})

Vue.filter('pickColor', function (dataStr) {
  /**
   * 返回一个颜色rgb
   */
  const colors = ['pink', 'red', 'orange', 'green', 'cyan', 'blue', 'purple', 'pink', 'red', 'orange']
  if (!dataStr) {
    return 'blue'
  }

  let index = dataStr.toString().slice(-1)
  if (Number(index) > 0) {
    return colors[index]
  } else {
    const char1 = String(index.charCodeAt())
    const index2 = dataStr.toString().slice(0, 1)
    const char2 = String(index2.charCodeAt())
    index = (char1 + char2).slice(-1)
    return colors[index]
  }
})
