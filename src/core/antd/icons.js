export {
  default as SettingOutline
} from '@ant-design/icons/lib/outline/SettingOutline'
export {
  default as GithubOutline
} from '@ant-design/icons/lib/outline/GithubOutline'
export {
  default as CopyrightOutline
} from '@ant-design/icons/lib/outline/CopyrightOutline'

/* MultiTab begin */
export {
  default as CloseOutline
} from '@ant-design/icons/lib/outline/CloseOutline'
export {
  default as ReloadOutline
} from '@ant-design/icons/lib/outline/ReloadOutline'
export {
  default as DeleteOutline
} from '@ant-design/icons/lib/outline/DeleteOutline'
export {
  default as DownOutline
} from '@ant-design/icons/lib/outline/DownOutline'
export {
  default as UpOutline
} from '@ant-design/icons/lib/outline/UpOutline'
export {
  default as AlignLeftOutline
} from '@ant-design/icons/lib/outline/AlignLeftOutline'
/* MultiTab end */

/* Layout begin */
export {
  default as LeftOutline
} from '@ant-design/icons/lib/outline/LeftOutline'
export {
  default as RightOutline
} from '@ant-design/icons/lib/outline/RightOutline'
export {
  default as MenuFoldOutline
} from '@ant-design/icons/lib/outline/MenuFoldOutline'
export {
  default as MenuUnfoldOutline
} from '@ant-design/icons/lib/outline/MenuUnfoldOutline'
export {
  default as DashboardOutline
} from '@ant-design/icons/lib/outline/DashboardOutline'
export {
  default as VideoCameraOutline
} from '@ant-design/icons/lib/outline/VideoCameraOutline'
export {
  default as LoadingOutline
} from '@ant-design/icons/lib/outline/LoadingOutline'
export {
  default as GlobalOutline
} from '@ant-design/icons/lib/outline/GlobalOutline'
export {
  default as UserOutline
} from '@ant-design/icons/lib/outline/UserOutline'
export {
  default as LogoutOutline
} from '@ant-design/icons/lib/outline/LogoutOutline'
/* Layout end */
export {
  default as SlackOutline
} from '@ant-design/icons/lib/outline/SlackOutline'
export {
  default as FileTextOutline
} from '@ant-design/icons/lib/outline/FileTextOutline'
export {
  default as EditOutline
} from '@ant-design/icons/lib/outline/EditOutline'
export {
  default as SearchOutline
} from '@ant-design/icons/lib/outline/SearchOutline'
export {
  default as BellOutline
} from '@ant-design/icons/lib/outline/BellOutline'
export {
  default as QuestionCircleOutline
} from '@ant-design/icons/lib/outline/QuestionCircleOutline'
export {
  default as InfoCircleOutline
} from '@ant-design/icons/lib/outline/InfoCircleOutline'
export {
  default as ExclamationOutline
} from '@ant-design/icons/lib/outline/ExclamationOutline'
export {
  default as CheckCircleOutline
} from '@ant-design/icons/lib/outline/CheckCircleOutline'
export {
  default as CloseCircleOutline
} from '@ant-design/icons/lib/outline/CloseCircleOutline'
export {
  default as CloseCircleFill
} from '@ant-design/icons/lib/fill/CloseCircleFill'
export {
  default as ExclamationCircleFill
} from '@ant-design/icons/lib/fill/ExclamationCircleFill'
export {
  default as CaretDownOutline
} from '@ant-design/icons/lib/outline/CaretDownOutline'
export {
  default as CheckCircleFill
} from '@ant-design/icons/lib/fill/CheckCircleFill'
export {
  default as CaretDownFill
} from '@ant-design/icons/lib/fill/CaretDownFill'
export {
  default as SmileOutline
} from '@ant-design/icons/lib/outline/SmileOutline'
export {
  default as PlusOutline
} from '@ant-design/icons/lib/outline/PlusOutline'

export {
  default as AreaChartOutline
} from '@ant-design/icons/lib/outline/AreaChartOutline'
export {
  default as PieChartOutline
} from '@ant-design/icons/lib/outline/PieChartOutline'
export {
  default as BarChartOutline
} from '@ant-design/icons/lib/outline/BarChartOutline'
export {
  default as LineChartOutline
} from '@ant-design/icons/lib/outline/LineChartOutline'
export {
  default as RadarChartOutline
} from '@ant-design/icons/lib/outline/RadarChartOutline'

export {
  default as FileDoneOutline
} from '@ant-design/icons/lib/outline/FileDoneOutline'
export {
  default as ProfileOutline
} from '@ant-design/icons/lib/outline/ProfileOutline'
