import { axios } from '@/utils/request'

import api from './index'

export function fetchMissionApply (params) {
  return axios({
    url: api.MissionApply,
    method: 'get',
    params
  })
}

export function addMissionApply (data) {
  return axios({
    url: api.MissionApply,
    method: 'post',
    data
  })
}

export function updateMissionApply (id, data) {
  return axios({
    url: `${api.MissionApply}/${id}`,
    method: 'put',
    data
  })
}

export function deleteMissionApply (id) {
  return axios({
    url: `${api.MissionApply}/${id}`,
    method: 'delete'
  })
}

export function allMissionApply (id) {
  return axios({
    url: `${api.ChildMission}/all`,
    method: 'get'
  })
}
