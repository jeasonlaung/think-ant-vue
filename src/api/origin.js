import { axios } from '@/utils/request'

import api from './index'

export function fetchOrigin (params) {
  return axios({
    url: api.Origin,
    method: 'get',
    params
  })
}

export function addOrigin (data) {
  return axios({
    url: api.Origin,
    method: 'post',
    data
  })
}

export function updateOrigin (id, data) {
  return axios({
    url: `${api.Origin}/${id}`,
    method: 'put',
    data
  })
}

export function deleteOrigin (id) {
  return axios({
    url: `${api.Origin}/${id}`,
    method: 'delete'
  })
}

export function allOrigin () {
  return axios({
    url: `${api.Origin}/all`,
    method: 'get'
  })
}
