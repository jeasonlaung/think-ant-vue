import { axios } from '@/utils/request'

import api from './index'

export function fetchMainMission (params) {
  return axios({
    url: api.MainMission,
    method: 'get',
    params
  })
}

export function addMainMission (data) {
  return axios({
    url: api.MainMission,
    method: 'post',
    data
  })
}

export function updateMainMission (id, data) {
  return axios({
    url: `${api.MainMission}/${id}`,
    method: 'put',
    data
  })
}

export function deleteMainMission (id) {
  return axios({
    url: `${api.MainMission}/${id}`,
    method: 'delete'
  })
}

export function allMainMission (params) {
  return axios({
    url: `${api.MainMission}/all`,
    method: 'get',
    params
  })
}
