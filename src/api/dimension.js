import { axios } from '@/utils/request'

import api from './index'

export function fetchDimension (params) {
  return axios({
    url: api.Dimension,
    method: 'get',
    params
  })
}

export function addDimension (data) {
  return axios({
    url: api.Dimension,
    method: 'post',
    data
  })
}

export function updateDimension (id, data) {
  return axios({
    url: `${api.Dimension}/${id}`,
    method: 'put',
    data
  })
}

export function deleteDimension (id) {
  return axios({
    url: `${api.Dimension}/${id}`,
    method: 'delete'
  })
}

export function allDimension (id) {
  return axios({
    url: `${api.Dimension}/all`,
    method: 'get'
  })
}
