import { axios } from '@/utils/request'

import api from './index'

export function fetchMission (params) {
  return axios({
    url: api.Mission,
    method: 'get',
    params
  })
}

export function addMission (data) {
  return axios({
    url: api.Mission,
    method: 'post',
    data
  })
}

export function updateMission (id, data) {
  return axios({
    url: `${api.Mission}/${id}`,
    method: 'put',
    data
  })
}

export function deleteMission (id) {
  return axios({
    url: `${api.Mission}/${id}`,
    method: 'delete'
  })
}

export function allMission (id) {
  return axios({
    url: `${api.Mission}/all`,
    method: 'get'
  })
}
