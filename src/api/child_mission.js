import { axios } from '@/utils/request'

import api from './index'

export function fetchDayMission (params) {
  return axios({
    url: api.Mission + '/myDay',
    method: 'get',
    params
  })
}

export function fetchMyMission (params) {
  return axios({
    url: api.Mission + '/my',
    method: 'get',
    params
  })
}
export function fetchPlatformMission (params) {
  return axios({
    url: api.Mission + '/platform',
    method: 'get',
    params
  })
}

export function addMission (data) {
  return axios({
    url: api.Mission,
    method: 'post',
    data
  })
}

export function updateMission (id, data) {
  return axios({
    url: `${api.Mission}/${id}`,
    method: 'put',
    data
  })
}

export function deleteMission (id) {
  return axios({
    url: `${api.Mission}/${id}`,
    method: 'delete'
  })
}

export function allMission (id) {
  return axios({
    url: `${api.Mission}/all`,
    method: 'get'
  })
}
