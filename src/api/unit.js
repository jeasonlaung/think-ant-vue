import { axios } from '@/utils/request'

import api from './index'

export function fetchUnit (params) {
  return axios({
    url: api.Unit,
    method: 'get',
    params
  })
}

export function addUnit (data) {
  return axios({
    url: api.Unit,
    method: 'post',
    data
  })
}

export function updateUnit (id, data) {
  return axios({
    url: `${api.Unit}/${id}`,
    method: 'put',
    data
  })
}

export function deleteUnit (id) {
  return axios({
    url: `${api.Unit}/${id}`,
    method: 'delete'
  })
}

export function allUnit () {
  return axios({
    url: `${api.Unit}/all`,
    method: 'get'
  })
}
