const api = {
  Login: '/admin/auth/login',
  Logout: '/admin/auth/logout',
  RefreshToken: '/admin/auth/refresh_token',
  ForgePassword: '/admin/auth/forge-password',
  Register: '/admin/auth/register',
  twoStepCode: '/admin/auth/2step-code',
  SendSms: '/admin/account/sms',
  SendSmsErr: '/admin/account/sms_err',
  // get my info
  UserInfo: '/admin/user/info',

  Role: '/admin/role',
  Menu: '/admin/menu',
  Action: '/admin/action',
  User: '/admin/user',
  UserCurrent: '/admin/user/current',
  ResetPassword: '/admin/user/reset-password',
  Avatar: '/admin/user/avatar',
  Dept: '/admin/dept',
  Post: '/admin/system/post',
  Log: {
    Acount: '/admin/log/acount',
    Db: '/admin/log/db'
  },
  ArticleCategory: '/admin/article/category',
  Article: '/admin/article',

  // 维度
  Dimension: '/admin/dimension',
  // 子项目
  Mission: '/admin/mission',
  // 申请
  MissionApply: '/admin/mission_apply',
  // 主项单
  MainMission: '/admin/mission_main',
  Check: '/admin/check',
  Origin: '/admin/origin',
  Unit: '/admin/unit'
}
export default api
