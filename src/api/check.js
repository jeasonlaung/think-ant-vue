import { axios } from '@/utils/request'

import api from './index'

export function fetchCheck (params) {
  return axios({
    url: api.Check,
    method: 'get',
    params
  })
}

export function addCheck (data) {
  return axios({
    url: api.Check,
    method: 'post',
    data
  })
}

export function updateCheck (id, data) {
  return axios({
    url: `${api.Check}/${id}`,
    method: 'put',
    data
  })
}

export function deleteCheck (id) {
  return axios({
    url: `${api.Check}/${id}`,
    method: 'delete'
  })
}

export function allCheck () {
  return axios({
    url: `${api.Check}/all`,
    method: 'get'
  })
}
