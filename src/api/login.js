import api from './index'
import { axios } from '@/utils/request'
import router from '@/router/index'
import { ACCESS_TOKEN } from '@/store/mutation-types'

/**
 * login func
 * parameter: {
 *     username: '',
 *     password: '',
 *     remember_me: true,
 *     captcha: '12345'
 * }
 * @param parameter
 * @returns {*}
 */
export function login (parameter) {
  return axios({
    url: api.Login,
    method: 'post',
    data: parameter
  })
}

export function getSmsCaptcha (parameter) {
  return axios({
    url: api.SendSms,
    method: 'post',
    data: parameter
  })
}

export function getInfo (params) {
  return new Promise((resolve, reject) => {
    axios({
      url: api.UserInfo,
      method: 'get',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8'
      },
      params
    }).then(data => resolve(data))
    .catch(e => {
      reject(e)
      window.localStorage.removeItem(ACCESS_TOKEN)
      router.replace('/user/login')
    })
  })
}

export function logout () {
  return axios({
    url: api.Logout,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

/**
 * get user 2step code open?
 * @param parameter {*}
 */
export function get2step (parameter) {
  return axios({
    url: api.twoStepCode,
    method: 'post',
    data: parameter
  })
}

export function refreshToken (params) {
  return axios({
    url: api.RefreshToken,
    method: 'get',
    params
  })
}
